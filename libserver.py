#!/usr/bin/python3.5

import os
# import sys
import time
from configparser import ConfigParser

from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler


user_home = os.path.expanduser('~')


class LibraryHandler(PatternMatchingEventHandler):
    def __init__(self):
        PatternMatchingEventHandler.__init__(self)

        self.library_list = dict()
        self.library_file_path = \
            os.path.join(user_home, 'dotfiles', 'libraries')

    def on_deleted(self, event):
        library_name = os.path.basename(event.src_path)

        try:
            del self.library_list[library_name]
        except KeyError:
            pass

    def on_created(self, event):
        if os.path.isfile(event.src_path):
            print('files currently not supported, ignoring it ...')
            return

        library_path = os.path.abspath(event.src_path)
        library_name = os.path.basename(event.src_path)

        directories = os.listdir(library_path)

        url = ''
        if '.git' in directories:
            library_git_conf_path = os.path.join(library_path, '.git/config')
            with open(library_git_conf_path, 'r') as f:
                cp = ConfigParser()
                cp.read_file(f)
                url = cp['remote "origin"']['url']

        build = ''
        if 'CMakeLists.txt' in directories:
            build = 'cmake'
        elif set(('Makefile', 'configure')).intersection(directories):
            build = 'make'

        self.library_list[library_name] = {'url': url, 'build': build}
        self.update_library()

    def on_modified(self, event):
        print('modified')

    def update_library(self):
        with open(self.library_file_path, "w+") as f:
            for k, v in self.library_list.items():
                f.write("{}: {}\n".format(k, v))


if __name__ == '__main__':
    observer = Observer()
    observer.schedule(
            LibraryHandler(),
            path=os.path.join(user_home, 'libraries'),
            recursive=True)

    os.chdir(user_home)
    observer.start()

    try:
        while True:
            time.sleep(600)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
